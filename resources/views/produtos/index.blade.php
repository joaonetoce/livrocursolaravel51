
<div class="container">
    <h1>Lista de produtos</h1>

    <a href="{{route('produtos.create')}}" class="btn btn-default"> Novo Produto</a>

    <table class="table table-bordered table-responsive">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Descrição</th>
                <th>Ação</th>
            </tr>
        </thead>

        <tbody>
            @foreach($produtos as $produto)
                <tr>
                    <td>{{$produto->id}}</td>
                    <td>{{$produto->nome}}</td>
                    <td>{{$produto->descricao}}</td>
                    <td>
                        <a href="{{route('produtos.edit', ['id'=>$produto->id])}}" class="btn btn-sm btn-success">Editar</a> /
                        <a href="{{route('produtos.destroy', ['id'=>$produto->id])}}" class="btn btn-sm, btn-danger">Remover</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <ul>

    </ul>
</div>





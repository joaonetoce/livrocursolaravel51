<?php
/**
 * Created by IntelliJ IDEA.
 * User: joao
 * Date: 03/10/2016
 * Time: 10:17
 */

namespace App\Repositories;


abstract class AbstractRepository implements Contracts\RepositoryInterface
{
    protected $model;

    public function getAll()
    {
        return $this->model->all();
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function update($id, array $data)
    {
        return $this->model->find($id)->update($data);
    }

    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }
}
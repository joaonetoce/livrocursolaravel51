<?php
/**
 * Created by IntelliJ IDEA.
 * User: joao
 * Date: 03/10/2016
 * Time: 10:14
 */

namespace App\Repositories\Contracts;


interface RepositoryInterface
{
    public function getAll();
    public function find($id);
    public function create(array $data);
    public function update($id, array $data);
    public function delete($id);
}
<?php

namespace App\Repositories\Eloquent;

use App\Produto;
use App\Repositories\AbstractRepository;
use App\Repositories\Contracts\ProdutoRepositoryInterface;

/**
 * Created by IntelliJ IDEA.
 * User: joao
 * Date: 03/10/2016
 * Time: 10:03
 */
class ProdutoRepository extends AbstractRepository implements ProdutoRepositoryInterface
{
    public function __construct(Produto $model)
    {
        $this->model = $model;
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function update($id, array $data)
    {
        return $this->model->find($id)->update($data);
    }

    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }
}